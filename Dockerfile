#
## @brief Dockerfile permettant la construction d'une image ElasticSearch
## @author Joel LE CORRE <jlecorre+git@sublimigeek.fr>
#

# Recuperation de l'image sbgk-java8u60
FROM sbgk-java:8u74

# Informations concernant le mainteneur de l'image et du Dockerfile
MAINTAINER "Joel LE CORRE <jlecorre+git@sublimigeek.fr>"

# Initialisation de variables pour la construction du container
ENV es_version 2.2.0
ENV es_binary elasticsearch-${es_version}.tar.gz
ENV es_sha1 ${es_binary}.sha1
ENV work_dir /opt
ENV es_home ${work_dir}/elasticsearch-${es_version}
ENV es_link https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/${es_version}/${es_binary}
ENV es_link_sha1 https://download.elasticsearch.org/elasticsearch/release/org/elasticsearch/distribution/tar/elasticsearch/${es_version}/${es_binary}.sha1
ENV wget_options --no-verbose --no-check-certificate

# Mise a jour de l'image
RUN \
  apt-get -y update && apt-get -y dist-upgrade

# Creation de l'utilisateur elastic (autorise a demarrer le service)
# /!\ L'utilisateur, son UID et son GID doivent exister sur la machine hote /!\
RUN \
  groupadd -g 1001 -r elastic && \
  useradd -u 1001 -r -g elastic elastic

# Creation des repertoires et des permissions utiles a la construction de l'image
RUN \
  mkdir -p ${es_home}

# Workspace utilise pour la construction de l'image
WORKDIR ${work_dir}

# Recuperation de sources a installer
RUN \
  wget ${wget_options} ${es_link} -P ${work_dir} && \
  wget ${wget_options} ${es_link_sha1} -P ${work_dir} -O ${es_sha1}

# Controle du sha1 du binaire telecharge depuis la source officielle
ADD scripts/elastic_check_sha1.sh /${work_dir}/elastic_check_sha1.sh
RUN \
  ./elastic_check_sha1.sh

# Decompression des sources et suppression des fichiers inutiles
RUN \
  tar xfz ${work_dir}/${es_binary} -C ${work_dir} && \
  rm -f ${work_dir}/${es_binary}

# Workspace utilise pour la construction de l'image
WORKDIR ${es_home}

# Installation des plugins ElasticSearch
RUN \
  # Installation du plugin Marvel pour ElasticSearch v2.2.0
  ./bin/plugin install license && \
  ./bin/plugin install marvel-agent && \
  # Installation de plugins tiers
  ./bin/plugin install royrusso/elasticsearch-HQ && \
  ./bin/plugin install lmenezes/elasticsearch-kopf/latest && \
  ./bin/plugin install mobz/elasticsearch-head

### Plugins non compatibles avec ElasticSearch v2.1.1 (temporairement ??)
#./bin/plugin install lukas-vlcek/bigdesk && \
#./bin/plugin install andrewvc/elastic-hammer && \
#./bin/plugin install polyfractal/elasticsearch-inquisitor && \
#./bin/plugin install karmi/elasticsearch-paramedic && \
#./bin/plugin install polyfractal/elasticsearch-segmentspy && \
#./bin/plugin install xyu/elasticsearch-whatson/0.1.3 && \
#./bin/plugin install org.codelibs/elasticsearch-reindexing/1.4.2

# Ajout de la configuration d'ElasticSearch dans l'image
ADD config/elasticsearch.yml ${es_home}/config/elasticsearch.yml
ADD scripts/elastic_init.sh ${es_home}/bin/elastic_init.sh

# Definition des permissions
RUN \
  chown -R elastic:elastic ${es_home}

# Definition de la commande executee par defaut
USER elastic
CMD ${es_home}/bin/elastic_init.sh

# Liste des ports que l'on expose
#   - 9200: HTTP
#   - 9300: transport
EXPOSE 9200
EXPOSE 9300
