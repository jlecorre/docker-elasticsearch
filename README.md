ElasticSearch Dockerfile
=================

Short description
-----------------
Elasticsearch is a highly scalable open-source full-text search and analytics engine.
It allows you to store, search, and analyze big volumes of data quickly and in near real time.
It is generally used as the underlying engine/technology that powers applications that have complex search features and requirements.

How to build image
------------------
Get the code and build your own image :
```
git clone ${repository}
cd ${repository}
docker build -t ${docker_image_name}:${docker_tag_name} .
```

ENJOY ;)

