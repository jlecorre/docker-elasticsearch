#!/bin/bash

#
## @file elastic_init.sh
## @author Joel LE CORRE <jlecorre+git@sublimigeek.fr>
## @brief Execute le binaire elasticsearch
## @needs Plusieurs variables d'environnement initialisees lors de la creation du conteneur
#

# Demarrage du binaire Elasticsearch
if [[ -z "$JMX_JAVA_OPTS" ]]
then
    ${es_home}/bin/elasticsearch
else
    ${es_home}/bin/elasticsearch $JMX_JAVA_OPTS
fi
