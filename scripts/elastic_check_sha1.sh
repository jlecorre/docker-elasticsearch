#!/bin/bash

#
## @file elastic_check_sha1.sh
## @author Joel LE CORRE <jlecorre+git@sublimigeek.fr>
## @brief Controle la somme du SHA1 du binaire Elasticsearch recupere lors de la construction de l'image
## @needs Le binaire ElasticSearch ainsi que la SHA1 genere par elastic.co
#

# Definition des variables utiles au script de controle (basees sur ENV de l'image)
es_binary_sha1="$(sha1sum ${work_dir}/${es_binary} | cut -d ' ' -f1)"
es_checksum_sha1="$(cat ${work_dir}/${es_sha1})"

# Verification du SHA1 et affichage d'un message lors de la construction de l'image
if [ "${es_binary_sha1}" == "${es_checksum_sha1}" ]
then
  echo -e "\n\n########################"
  echo -e "## SHA1SUM CHECK [OK] ##"
  echo -e "########################\n\n"
else
  echo -e "\n\n##########################"
  echo -e "### SHA1SUM CHECK [KO] ###"
  echo -e "## Don't use this image ##"
  echo -e "##########################\n\n"
fi
